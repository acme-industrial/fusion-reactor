let modules;
let cores;
let active = false;
const api = {
  'replace':coreID => {
    if (cores.has(coreID)) {
      cores.delete();
      modules.core.new();
    }
  },
  'activate':coreID => {
    if (!cores.has(coreID)) {
      cores.set(coreID, 0);
    }
  },
  'set':is => {
    active = is ? true:false;
  }
}
module.exports = {
  'name':'core',
  'execute':m => {
    const send = (channel, count) => {
      const degrees = Math.round((count / 9000) * 150000000);
      const content = `[@everyone] ${degrees} °C`;
      const author =  `Core Temperature`;
      const avatar = `https://cdn.discordapp.com/attachments/296107530915151872/594370886853722113/reactor.jpg`;
      channel.fetchWebhooks().then(hooks => {
        if (hooks.size > 0) {
          let hook = hooks.find(h => h.owner.id == modules.client.user.id);
          if (hook) {
            hook.send(content, {'username':author})
          } else {
            channel.createWebhook('ReactorCore', avatar).then(hook => {
              hook.send(content, {'username':author})
            });
          };
        } else {
          channel.createWebhook('ReactorCore', avatar).then(hook => {
            hook.send(content, {'username':author})
          });
        };
      });
    }
    modules = m;
    cores = new m.client.discord.Collection();
    setInterval(() => {//change to separately threaded loops?
      for (const coreID of cores.keyArray()) {
        if (!modules.client.channels.has(coreID)) return;
        const channelPingCount = cores.get(coreID);
        if (channelPingCount <= 9000) {
          if (active) {
            cores.set(coreID, cores.get(coreID) + 1);
            send(modules.client.channels.get(coreID), channelPingCount);
          }
        } else {
          api.replace(coreID);
        }
      }
    }, 1000 / modules.config.clock);
    modules.client.on('ready', () => {
      const children = modules.client.channels.get(modules.config.category).children;
      if (children) {
        for (const channel of children.array()) {
          api.activate(channel.id);
        }
      }
    })
  },
  'list':() => cores,
  'new':(amount) => {
    if (cores.size) return cores.size;
    amount = amount || 1;
    for (let i = 0; i < amount; i++) {
      if (cores.size < modules.config.reactors) {
        const num = i + 1;
        modules.client.channels.get(modules.config.category).guild.createChannel(`core ${num}`, {'parent':modules.config.category, 'topic':`reactor core #${num}, must reach 150,000,000 °C`}).then(channel => {
          api.activate(channel.id);
        })
      } else console.log(`failed to initialize a core, surpasses limit`);
    }
  },
  'set':api.set,
  'clear':() => {
    api.set(false);
    let promises = [];
    for (const channel of cores.keyArray().map(channelID => modules.client.channels.get(channelID))) {
      cores.delete(channel.id);
      promises.push(channel.delete());
    }
    Promise.all(promises).then(() => {
      console.log(`cleared ${promises.length} cores`);
    })
  }
}