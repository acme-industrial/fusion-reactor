const discord = require('discord.js');
const client = new discord.Client();
client.name = 'client';
client.discord = discord;
client.execute = modules => {
  client.login(modules.config.token);
  client.on('ready', () => console.log(`reactor technician [${client.user.tag}] logged in`));
}
module.exports = client;