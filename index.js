const fs = require('fs');
const load = () => {
  const modules = {};
  const files = fs.readdirSync('./modules').filter(file => file.endsWith('.js'));
  const err = (e, file) => {
    console.log(`fatal error in ${file}, exiting...`);
    console.error(e);
    process.exit();
  }
  for (const file of files) {
    try {
	    const command = require(`./modules/${file}`);
	    modules[command.name] = command;
    }
    catch (e) {
      err(e, file);
    }
  }
  for (const module of Object.values(modules)) {
    if (module.execute) {
      try {
        module.execute(modules);
      }
      catch (e) {
        err(e, module.name);
      }
    };
  }
}
console.log(`Preparing magnetic nuclei confinement system`);
load();