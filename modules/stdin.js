const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})
readline.name = 'stdin';
readline.execute = modules => {
  const commands = modules.cmd.all;
  modules.client.on('ready', () => {
    readline.on('line', (input) => {
      const args = input.split(/ +/);
      if (args[0] in commands) {
        commands[args[0]](input, args.slice(1), modules);
      } else {
        console.log(`${args[0]} is not a recognized command`);
        commands['help'](input, args.slice(1), modules);
      }
    }).on('close', () => {
      commands['exit']();
    });
  })
}
module.exports = readline;