module.exports = {
  'name':'cmd',
  'all':{
    'exit':(stdin, args, modules) => {
      console.log(`shutting down reactor cores...`);
      process.exit();
    },
    'help':(stdin, args, modules) => {
      console.log(Object.keys(modules.cmd.all).join(', '));
    },
    'start':(stdin, args, modules) => {
      let count = args[0] || modules.config.reactors;
      modules.core.new(count);
      modules.core.set(true);
    },
    'clear':(stdin, args, modules) => {
      modules.core.clear();
    },
    'stop':(stdin, args, modules) => {
      modules.core.set(false);
    }
  }
}